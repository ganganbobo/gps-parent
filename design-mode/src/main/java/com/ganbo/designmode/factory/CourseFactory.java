package com.ganbo.designmode.factory;

/**
 * Created by gan on 2019/10/29 21:03.
 */
public class CourseFactory {

    public static ICourse courseFactory(String name) {
        if ("java".equals(name)) {
            return new JavaCourse();
        } else {
            return new PythonCourse();
        }
    }
}
