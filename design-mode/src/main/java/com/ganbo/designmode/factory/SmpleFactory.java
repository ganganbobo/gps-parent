package com.ganbo.designmode.factory;

/**
 * Created by gan on 2019/10/27 22:59.
 */
public class SmpleFactory {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Course course = courseFactory(Course.class.getName());
        System.out.println(course.getName());

    }

    public static Course courseFactory(String simpleName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        return (Course) Class.forName(simpleName).newInstance();
    }


}
