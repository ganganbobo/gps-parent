package com.ganbo.designmode.factory;

import lombok.Data;

/**
 * Created by gan on 2019/10/27 22:56.
 */

@Data
public class Course {

    private Long id;
    private String name;
}
