package com.ganbo.designmode.factory4;

/**
 * 具体工厂B负责具体的产品B生产
 */
public class ProductBFactory implements Factory {
    @Override
    public IProduct getProduct() {
        return new ProductB();
    }
}
