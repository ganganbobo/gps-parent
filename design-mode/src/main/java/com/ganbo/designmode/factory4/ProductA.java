package com.ganbo.designmode.factory4;

/**
 * 具体的饮料A(果汁)
 */
public class ProductA implements IProduct {
    @Override
    public void intro() {
        System.out.println("果汁");
    }
}
