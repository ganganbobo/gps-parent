package com.ganbo.designmode.factory4;

/**
 * 具体工厂A负责具体的产品A生产
 */
public class ProductAFactory implements Factory {
    @Override
    public IProduct getProduct() {
        return new ProductA();
    }
}
