package com.ganbo.designmode.factory4;


/**
 * 抽象产品
 */
public interface IProduct {
    //产品介绍
    void intro();
}
