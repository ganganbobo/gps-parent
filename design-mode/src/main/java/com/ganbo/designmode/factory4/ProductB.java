package com.ganbo.designmode.factory4;

/**
 * 具体饮料B(雪碧)
 */
public class ProductB implements IProduct {
    @Override
    public void intro() {
        System.out.println("雪碧");
    }
}
