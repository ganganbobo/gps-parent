package com.ganbo.designmode.factory4;

/**
 * 抽象工厂
 */
public interface Factory {
    IProduct getProduct();
}
