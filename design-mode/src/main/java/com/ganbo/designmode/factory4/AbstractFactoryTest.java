package com.ganbo.designmode.factory4;

/**
 * 测试
 */
public class AbstractFactoryTest {
    public static void main(String[] args) {
        Factory factoryA = new ProductAFactory();
        Factory factoryB = new ProductBFactory();
        IProduct producta = factoryA.getProduct();
        IProduct productb = factoryB.getProduct();
        producta.intro();
        productb.intro();
    }
}
