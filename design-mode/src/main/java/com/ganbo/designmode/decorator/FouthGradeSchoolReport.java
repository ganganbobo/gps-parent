package com.ganbo.designmode.decorator;

/**
 * Created by gan on 2020/4/6 22:39.
 */
public class FouthGradeSchoolReport extends SchoolReport {

    @Override
    void report() {
        System.out.println("尊敬的xxx家长:");
        System.out.println("    语文62  数学70 自然60");
        System.out.println("    .....");
        System.out.println("家长签名:");
    }

    @Override
    void sign(String name) {
        System.out.println("家长签名为:" + name);
    }
}
