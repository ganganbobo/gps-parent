package com.ganbo.designmode.decorator;

/**
 * Created by gan on 2020/4/6 22:45.
 */
public class Father {
    public static void main(String[] args) {

        SchoolReport schoolReport = new FouthGradeSchoolReport();

        HighScoreDecorator highScoreDecorator = new HighScoreDecorator(schoolReport);

        SortDecorator sortDecorator = new SortDecorator(highScoreDecorator);

        sortDecorator.report();
        sortDecorator.sign("张三");
    }
}
