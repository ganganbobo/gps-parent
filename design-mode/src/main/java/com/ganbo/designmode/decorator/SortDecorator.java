package com.ganbo.designmode.decorator;

/**
 * Created by gan on 2020/4/6 22:44.
 */
public class SortDecorator extends SchoolReportDecorator {

    public SortDecorator(SchoolReport schoolReport) {
        super(schoolReport);
    }

    @Override
    void sign(String name) {
        System.out.println("本次考试排名情况是:30名。");
        super.sign(name);
    }
}


