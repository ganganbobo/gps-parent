package com.ganbo.designmode.decorator;

/**
 * Created by gan on 2020/4/6 22:38.
 */
public abstract class SchoolReport {

    abstract void report();

    abstract void sign(String name);
}
