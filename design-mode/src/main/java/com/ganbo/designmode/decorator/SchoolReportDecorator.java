package com.ganbo.designmode.decorator;

import lombok.AllArgsConstructor;

/**
 * Created by gan on 2020/4/6 22:41.
 */

@AllArgsConstructor
public class SchoolReportDecorator extends SchoolReport {

    private SchoolReport schoolReport;

    @Override
    void report() {
        schoolReport.report();
    }

    @Override
    void sign(String name) {
        schoolReport.sign(name);
    }
}
