package com.ganbo.designmode.decorator;

/**
 * Created by gan on 2020/4/6 22:43.
 */
public class HighScoreDecorator extends SchoolReportDecorator {

    public HighScoreDecorator(SchoolReport schoolReport) {
        super(schoolReport);
    }

    @Override
    void report() {
        System.out.println("本次考试最高分是: 80, 最低分是: 50");
        super.report();
    }
}
