package com.ganbo.designmode.singleton;

/**
 * 饿汉式（线程安全）
 * Created by gan on 2019/10/28 22:52.
 */
public class HungrySigleton {

    public static final  HungrySigleton  instance = new HungrySigleton();

    private HungrySigleton(){}

    public static HungrySigleton getInstance(){
        return instance;
    }
}
