package com.ganbo.designmode.singleton;

/**
 * 枚举单利模式
 * Created by gan on 2019/11/17 17:57.
 */
public enum EnumSingleton {
    INSTANCE;

    public void otherMetthod() {
        System.out.println("需要单利对象调用的方法。。。");
    }
}
