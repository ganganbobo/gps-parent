package com.ganbo.designmode.singleton;

/**
 * 静态内部类方式
 * Created by gan on 2019/11/17 17:46.
 */
public class StaticInnerClassSingleton {

    //构造方法私有化
    private StaticInnerClassSingleton() {
    }

    //内部类
    private static class HolderInnerClass {
        //需要提供单利对象的外部类作为静态属性加载的时候就初始化
        private static StaticInnerClassSingleton instance = new StaticInnerClassSingleton();

        static {
            System.out.println("Hello wortld 内部类被加载的了");
        }
    }

    //对外暴漏访问点
    public static StaticInnerClassSingleton getInstance() {
        return HolderInnerClass.instance;
    }

    public static void otherClassLoading() {
        System.out.println("调用其他方法。。。。");
    }


    public static void main(String[] args) {
        StaticInnerClassSingleton.otherClassLoading();
        StaticInnerClassSingleton instance = StaticInnerClassSingleton.getInstance();
        System.out.println(instance);
    }
}
