package com.ganbo.designmode.singleton;

/**
 * Created by gan on 2019/10/29 19:16.
 */
public class DoubleCheckSigleton {

    private static DoubleCheckSigleton instance;

    public DoubleCheckSigleton() {

    }

    public static DoubleCheckSigleton getInstance() {
        if (instance == null) {
            synchronized (DoubleCheckSigleton.class) {
                if (instance == null) {
                    instance = new DoubleCheckSigleton();
                }
            }
        }
        return instance;
    }


    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                DoubleCheckSigleton instance = DoubleCheckSigleton.getInstance();
                System.out.println(Thread.currentThread().getName() + "====>" + instance);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DoubleCheckSigleton instance = DoubleCheckSigleton.getInstance();
                System.out.println(Thread.currentThread().getName() + "====>" + instance);
            }
        }).start();

        System.out.println("main线程执行完毕。。。");
    }
}
