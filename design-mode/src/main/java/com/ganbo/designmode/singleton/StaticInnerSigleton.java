package com.ganbo.designmode.singleton;

/**
 * Created by gan on 2019/10/29 19:47.
 */
public class StaticInnerSigleton {

    private StaticInnerSigleton() {
    }

    private StaticInnerSigleton getInstance() {
        return HolderStaticInnerSigton.instance;
    }

    private static class HolderStaticInnerSigton {
        private static StaticInnerSigleton instance = new StaticInnerSigleton();
    }
}
