package com.ganbo.designmode.singleton;

/**
 * 线程安全的懒汉式单利模式
 *
 * Created by gan on 2019/11/17 17:33.
 */
public class LazySingleton {
    private static LazySingleton instance;

    //构造方法私有化
    private LazySingleton() {
    }

    public synchronized static LazySingleton getInstance() {
        if (instance != null) {
            instance = new LazySingleton();
        }
        return instance;
    }
}
