package com.ganbo.designmode.delagate;

/**
 * Created by gan on 2019/11/17 22:17.
 */
public class DelegateTestDemo {

    public static void main(String[] args) {
        Boss boss = new Boss();
        boss.commond("加密", new TeamLeader());
    }
}
