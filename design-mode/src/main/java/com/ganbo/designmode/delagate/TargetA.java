package com.ganbo.designmode.delagate;

/**
 * Created by gan on 2019/11/17 22:05.
 */
public class TargetA implements Itarget {

    @Override
    public void doSomething(String commond) {
        System.out.println("我是员工A,我擅长加密，我现在开始干：" + commond);
    }
}
