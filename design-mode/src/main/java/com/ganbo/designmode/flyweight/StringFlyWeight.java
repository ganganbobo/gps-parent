package com.ganbo.designmode.flyweight;

/**
 * Created by gan on 2020/4/7 21:44.
 */
public class StringFlyWeight {

    public static void main(String[] args) {
        String s1 = "和谐";
        String s2 = "社会";
        String s3 = "和谐社会";

        String s4 = s1 + s2;

        System.out.println(s3 == s4);
        s4 = (s1 + s2).intern();

        System.out.println(s3 == s4);
    }
}
