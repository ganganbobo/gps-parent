package com.ganbo.designmode.proxy.jdkproxy;

import com.ganbo.designmode.proxy.staticproxy.Developer;
import com.ganbo.designmode.proxy.staticproxy.Ideveloper;

/**
 * Created by gan on 2020/4/6 18:08.
 */
public class App {

    public static void main(String[] args) {
        Developer developer = new Developer("admin");
        Ideveloper enginnerProxy = (Ideveloper) new EnginnerProxy().getInstance(developer);
        enginnerProxy.writeCode();
    }
}
