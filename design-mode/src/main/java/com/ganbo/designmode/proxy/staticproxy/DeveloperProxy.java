package com.ganbo.designmode.proxy.staticproxy;

public class DeveloperProxy implements Ideveloper {

    private Ideveloper ideveloper;

    public DeveloperProxy(Ideveloper ideveloper) {
        this.ideveloper = ideveloper;
    }

    @Override
    public void writeCode() {
        writeLog();
        ideveloper.writeCode();
    }

    public void writeLog() {
        System.out.println("==============write documentation===");
    }
}
