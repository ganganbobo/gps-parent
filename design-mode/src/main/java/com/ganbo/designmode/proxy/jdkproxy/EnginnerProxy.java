package com.ganbo.designmode.proxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by gan on 2020/4/6 18:04.
 */
public class EnginnerProxy implements InvocationHandler {

    private Object targetObject;

    public Object getInstance(Object o) {
        this.targetObject = o;
        return Proxy.newProxyInstance(o.getClass().getClassLoader(), o.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Enginner write documentation...");
        Object res = method.invoke(targetObject, args);
        return res;
    }
}
