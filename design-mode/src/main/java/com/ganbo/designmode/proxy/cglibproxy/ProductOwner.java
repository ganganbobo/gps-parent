package com.ganbo.designmode.proxy.cglibproxy;

/**
 * Created by gan on 2020/4/6 18:13.
 */
public class ProductOwner {

    private String name;

    public ProductOwner(String name) {
        this.name = name;
    }


    public void writeCode() {
        System.out.println("Product writeCode....");
    }

}
