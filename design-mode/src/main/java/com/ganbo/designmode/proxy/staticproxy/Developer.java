package com.ganbo.designmode.proxy.staticproxy;


import lombok.Data;

@Data
public class Developer implements Ideveloper {

    private String name;

    public Developer(String name) {
        this.name = name;
    }

    @Override
    public void writeCode() {
        System.out.println(name + "=========write code.");
    }
}
