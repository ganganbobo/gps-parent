package com.ganbo.designmode.proxy.cglibproxy;
/**
 * Created by gan on 2020/4/6 18:17.
 */
public class App {

    public static void main(String[] args) {

        ProductOwner admin = (ProductOwner)new EnginnerCGlibProxy(new ProductOwner("admin")).createCgLibProxy();
        admin.writeCode();
    }
}
