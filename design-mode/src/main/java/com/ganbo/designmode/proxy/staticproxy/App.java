package com.ganbo.designmode.proxy.staticproxy;

/**
 * Created by gan on 2020/4/6 18:00.
 */
public class App {

    public static void main(String[] args) {

        Developer developer = new Developer("admin");

        DeveloperProxy developerProxy = new DeveloperProxy(developer);
        developerProxy.writeCode();

    }
}
