package com.ganbo.designmode.template;

/**
 * Created by gan on 2019/11/17 12:09.
 */
public abstract class Work {

    //定义算法步凑流程
    public void workDay() {

        //1:上班开机
        openComputer();
        //2:搬砖:写代码
        coding();
        //3:下班关机
        closeComputer();
    }

    //开机
    private final void openComputer() {
        System.out.println("到达公司，开机");
    }

    //写代码
    protected abstract void coding();

    //关机
    private final void closeComputer() {
        System.out.println("下班，关机");
    }

}
