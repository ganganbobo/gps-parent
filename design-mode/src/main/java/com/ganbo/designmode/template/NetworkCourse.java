package com.ganbo.designmode.template;

/**
 * Created by gan on 2019/11/17 10:43.
 */


public abstract class NetworkCourse {


    protected void createCourse() {
        liveVideo();
        postNote();
        postSource();

        if(netHomework()){
            checkHomework();
        }
    }


    abstract void checkHomework();

    protected boolean netHomework() {
        return false;
    }


    final void postSource() {
        System.out.println("提交源码");
    }

    final void postNote() {
        System.out.println("提交笔记");
    }

    final void liveVideo() {
        System.out.println("直播授课");
    }
}

