package com.ganbo.designmode.template;

/**
 * Created by gan on 2019/11/17 11:05.
 */
public class JavaClass extends NetworkCourse {

    private boolean checkHomeworkd;

    public JavaClass(boolean checkHomeworkd) {
        this.checkHomeworkd = checkHomeworkd;
    }

    @Override
    void checkHomework() {
        System.out.println("检查Java作业");
    }

    @Override
    protected boolean netHomework() {
        return checkHomeworkd;
    }

    public static void main(String[] args) {
        NetworkCourse javaClass = new JavaClass(true);
        javaClass.createCourse();
    }
}
