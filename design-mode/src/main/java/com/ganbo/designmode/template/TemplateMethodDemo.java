package com.ganbo.designmode.template;

/**
 * Created by gan on 2019/11/17 12:21.
 */
public class TemplateMethodDemo {

    public static void main(String[] args) {
        Work javaProgrammer = new JavaProgrammer();
        javaProgrammer.workDay();

        System.out.println("================");
        Work phpProgrammer = new PhpProgrammer();
        phpProgrammer.workDay();

    }
}
