package com.ganbo.designmode.factory2;

public class SmipleFactoryTest {


    public static void main(String[] args) {
        ICourse javaCourse = CourseFactory.create("java");
        ICourse pythonCourse = CourseFactory.create("python");

        javaCourse.study();
        pythonCourse.study();
    }

}
