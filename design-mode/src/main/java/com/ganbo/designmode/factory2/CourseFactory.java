package com.ganbo.designmode.factory2;

/**
 * Created by gan on 2020/3/10 21:59.
 */
public class CourseFactory {

    public static ICourse create(String clazzName) {

        if ("java".equals(clazzName)) {
            return new JavaCourse();
        } else if ("python".equals(clazzName)) {
            return new PythonCourse();
        }
        return null;
    }
}
