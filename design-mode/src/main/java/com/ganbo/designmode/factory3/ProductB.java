package com.ganbo.designmode.factory3;

/**
 * 具体产品B(咖啡)
 */
public class ProductB implements IProduct {
    @Override
    public void intro() {
        System.out.println("咖啡");
    }
}
