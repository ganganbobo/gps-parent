package com.ganbo.designmode.factory3;


/**
 * 具体产品C(奶茶)
 */
public class ProductC implements IProduct {
    @Override
    public void intro() {
        System.out.println("奶茶");
    }
}
