package com.ganbo.designmode.factory3;


/**
 * 抽象产品,描述产品的公共接口
 */
public interface IProduct {
    void intro();
}
