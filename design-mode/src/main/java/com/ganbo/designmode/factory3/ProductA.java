package com.ganbo.designmode.factory3;

/**
 * 具体产品A(可乐)
 */
public class ProductA implements IProduct {
    @Override
    public void intro() {
        System.out.println("可乐");
    }
}
