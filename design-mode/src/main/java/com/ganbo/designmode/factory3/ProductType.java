package com.ganbo.designmode.factory3;

import lombok.AllArgsConstructor;

/**
 * 产品类型
 */
@AllArgsConstructor
public enum ProductType {

    A("可乐"),
    B("咖啡"),
    C("奶茶");
    private String intro;
}
