package com.ganbo.designmode.factory3;

//测试
public class SmpleFactoryTest {
    public static void main(String[] args) {
        //根据传入的参数产生不同的产品实例
        IProduct productA = ProductFactory.getProduct(ProductType.A);
        IProduct productB = ProductFactory.getProduct(ProductType.B);
        IProduct productC = ProductFactory.getProduct(ProductType.C);

        productA.intro();
        productB.intro();
        productC.intro();
    }
}
