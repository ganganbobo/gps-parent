package com.ganbo.designmode.factory3;

/**
 * 工厂负责实现创建所有实例的内部逻辑，并提供一个外界调用的方法，创建所需的产品对象。
 */
public class ProductFactory {

    /**
     * 外界调用的方法(可以看做是对外提供的三个按钮)
     *
     * @param type 类型参数
     * @return 具体产品实例
     */
    public static IProduct getProduct(ProductType type) {

        switch (type) {
            case A:
                return new ProductA();
            case B:
                return new ProductB();
            case C:
                return new ProductC();
            default:
                return null;
        }
    }
}
